const Product = require("../models/Product");


module.exports.addProduct = (data) => {
	
	// if (data.isAdmin) {
		
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
	
		return newProduct.save().then((product, error) => {
			
			if (error) {
				return false;
			
			} else {
				return true;
			};
		});
	// };
	
	// let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	});
};


module.exports.getAllProducts = () => {
  return Product.find().then(result => {
    return result;
  });
};




module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


module.exports.updateProduct = (reqParams, data, user) => {

	if (data.user.isAdmin) {

		let updatedProduct ={
			name : data.name,
			description : data.description,
			price : data.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	};

	let message = Promise.resolve("User must be ADMIN!!!!!");
	return message.then((value) => {
		return {value};
	});
};








// module.exports.updateProduct = (reqParams, data) => {

// 	if (data.isAdmin) {

// 		let updatedProduct ={
// 			name : data.name,
// 			description : data.description,
// 			price : data.price
// 		};

// 		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
// 		.then((product, error) => {
// 			if (error) {
// 				return false;
// 			} else {
// 				return ("Updated!");
// 			}
// 		});
// 	};

// 	let message = Promise.resolve("User must be ADMIN to access this!");
// 	return message.then((value) => {
// 		return {value};
// 	});
// };


module.exports.archiveProduct= (reqParams, data) => {

	if (data.isAdmin){
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId,updateActiveField).then((product,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	});
};

module.exports.restoreProduct= (reqParams, data) => {
	if (data.isAdmin){

	let updateActiveField = {
		isActive: true
	};
	return Product.findByIdAndUpdate(reqParams.productId,updateActiveField).then((product,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	});

};