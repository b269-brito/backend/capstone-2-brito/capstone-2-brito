// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

// Database Connection
mongoose.connect("mongodb+srv://britzjamesjbb:admin123@zuitt-bootcamp.bnzycsq.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to cloud database!"));

// Server listening
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT|| 4000}`));

